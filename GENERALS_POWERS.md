# General's Powers

7 Promotion Points in total.

### Rank 1

<table style="text-align: center">
  <tr>
    <td>
      EMP<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/EMP_Pulse.webp" alt="EMP"/>
    </td>
    <td>
      Sneak Attack<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Sneak_Attack.webp" alt="Sneak Attack"/>
    </td>
    <td>
      &rarr;&nbsp;&nbsp;&nbsp;&nbsp;Paradrop<sup>1</sup>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <br>
      <img width="122" height="98" src="assets/powers/Paradrop.webp" alt="Paradrop"/>
    </td>
    <td>
      &rarr;&nbsp;&nbsp;&nbsp;&nbsp;Tank Drop<sup>2</sup>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <br>
      <img width="122" height="98" src="assets/powers/Tank_Drop.webp" alt="Tank Drop"/>
    </td>
  </tr>
</table>

### Rank 3

<table style="text-align: center">
  <tr>
    <td>
      Neutron Barrage<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/upgrades/Neutron_Shells.webp" alt="Neutron Barrage"/>
      <br>
      &darr;
    </td>
    <td>
      Anthrax Bomb<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Anthrax_Bomb.webp" alt="Anthrax Gamma Bomb"/>
      <br>
      &darr;
    </td>
    <td>
      Carpet Bomb<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Granger_Carpet_Bomb.webp" alt="Carpet Bomb"/>
      <br>
      &darr;
    </td>
    <td>
      A-10 Missile Strike<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/A-10_Strike_3.webp" alt="A-10 Missile Strike"/>
      <br>
      &darr;
    </td>
    <td>
      Fuel Air Bomb<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Fuel_Air_Bomb.webp" alt="Fuel Air Bomb"/>
      <br>
      &darr;
    </td>
  </tr>
  <tr>
    <td>
      Artillery Barrage<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Artillery_Barrage_3.webp" alt="Artillery Barrage"/>
      <br>
      &darr;
    </td>
    <td>
      Atomic Bomb<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/NukeDrop.webp" alt="Atomic Bomb"/>
      <br>
      &darr;
    </td>
    <td>
      Nuclear Carpet Bomb<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Nuke_Carpet_Bomb.webp" alt="Nuclear Carpet Bomb"/>
      <br>
      &darr;
    </td>
    <td>
      Spectre Gunship<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/units/Spectre_Gunship.webp" alt="Spectre Gunship"/>
      <br>
      &nbsp;
    </td>
    <td>
      MOAB<sup>1</sup>
      <br>
      <img width="122" height="98" src="assets/powers/MOAB.webp" alt="MOAB"/>
      <br>
      &nbsp;
    </td>
  </tr>
</table>

### Rank 5

<table style="text-align: center">
  <tr>
    <td>
      Nuclear Barrage<sup>2</sup>
      <br>
      <img width="122" height="98" src="assets/upgrades/High_Explosive_Warhead.webp" alt="Nuclear Barrage"/>
    </td>
    <td>
      Napalm Bomb<sup>2</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Napalm_Strike.webp" alt="Napalm Bomb"/>
    </td>
    <td>
      Super Carpet Bomb<sup>3</sup>
      <br>
      <img width="122" height="98" src="assets/powers/Nuke_Carpet_Bomb.webp" alt="Super Carpet Bomb"/>
    </td>
  </tr>
</table>

<sup>1</sup> costs 1 Promotion Point<br>
<sup>2</sup> costs 2 Promotion Points<br>
<sup>3</sup> costs 3 Promotion Points

### Fact Sheets

##### Paradrop

- 4x C-130 Hercules Transport Planes
    - 15x Missile Defender
    - 6x Mini-Gunner

##### Tank Drop

- 4x C-130 Hercules Transport Planes
    - 6x Paladin
    - 3x Avenger