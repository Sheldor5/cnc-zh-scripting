# Aircraft

#### Supply Center

<table style="text-align: center">
  <tr>
    <td>
       CH-47 Chinook<br><img width="122" height="98" src="assets/units/Chinook.webp" alt="Chinook"/>
    </td>
  </tr>
  <tr>
    <td>
      CH-47 Combat Chinook<br><img width="122" height="98" src="assets/units/Combat_Chinook.webp" alt="Combat Chinook"/>
    </td>
  </tr>
</table>

#### Airfield

<table style="text-align: center">
  <tr>
    <td>
      AH-1 Cobra<br><img width="122" height="98" src="assets/units/Stealth_Comanche.png" alt="Comanche"/>
    </td>
    <td>
      MiG 1.44 - Black Napalm<br><img width="122" height="98" src="assets/units/MiG.webp" alt="MiG"/>
    </td>
    <td>
      F-117 Nighthawk<br><img width="122" height="98" src="assets/units/Stealth_Fighter.webp" alt="Stealth Fighter"/>
    </td>
    <td>
      F-22 Raptor<br><img width="122" height="98" src="assets/units/Raptor.webp" alt="Raptor"/>
    </td>
    <td>
      F-16XL Aurora<br><img width="122" height="98" src="assets/units/Aurora_Alpha.webp" alt="Aurora Alpha"/>
    </td>
    <td>
      A-10 Thunderbolt II<br><img width="122" height="98" src="assets/units/A-10.webp" alt="A-10 Thunderbolt II"/>
    </td>
  </tr>
  <tr>
    <td>
      Ka-29 Assault Helix<br><img width="122" height="98" src="assets/units/Helix.webp" alt="Assault Helix"/>
    </td>
    <td>
      MiG 1.44 - Tactical Nuke<br><img width="122" height="98" src="assets/units/MiG.webp" alt="MiG"/>
    </td>
    <td>
      B-2 Spirit<br><img width="122" height="98" src="assets/units/MOAB.webp" alt="MOAB"/>
    </td>
    <td>
      F-22X King Raptor<br><img width="122" height="98" src="assets/units/King_Raptor.webp" alt="King Raptor"/>
    </td>
    <td>
      SR-72 Aurora Alpha<br><img width="122" height="98" src="assets/units/Hypersonic_Aurora_Alpha.webp" alt="Hypersonic Aurora Alpha"/>
    </td>
    <td>
      <img src="assets/star.png" alt="Hero"/> AC-130H Gunship<br><img width="122" height="98" src="assets/units/Spectre_Gunship.webp" alt="Spectre Gunship"/>
    </td>
  </tr>
</table>

### Fact Sheets

#### Carrier

<table>
  <tr>
    <th>CH-47 Chinook</th>
    <th>CH-47 Combat Chinook</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Chinook.webp" alt="Chinook"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Supply_Lines.webp" alt="Supply Lines"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            + Point Defense Laser<br>
            + Auto-Repair
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Combat_Chinook.webp" alt="Combat Chinook"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            5.000$
          </td>
          <td style="text-align: left">
            + Point Defense Laser<br>
            + Auto-Repair<br>
            + 10 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>2 Mini-Gunner + 4 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Supply Center</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 10.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Supply Center</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Attack Helicopter

<table>
  <tr>
    <th>AH-1 Cobra</th>
    <th>Ka-29 Assault Helix</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Stealth_Comanche.png" alt="Comanche"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/><img width="60" height="48" src="assets/upgrades/Rocket_Pods.webp" alt="Rocket Pods"/><br><img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/><img width="60" height="48" src="assets/upgrades/Stealth_Comanche.webp" alt="Stealth Comanche"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.500$
          </td>
          <td style="text-align: left">
              + Point Defense Laser
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Helix.webp" alt="Assault Helix"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Nuke_Bomb.webp" alt="Nuke Bomb"/>
            <img width="60" height="48" src="assets/upgrades/Helix_Gatling_Cannon.webp" alt="Gatling Cannon"/>
            <img width="60" height="48" src="assets/upgrades/Chainguns.webp" alt="Chainguns"/>
            <img width="60" height="48" src="assets/upgrades/Black_Napalm.webp" alt="Black Napalm"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Napalm_Bomb.webp" alt="Napalm Bomb"/>
            <img width="60" height="48" src="assets/upgrades/Helix_Battle_Bunker.webp" alt="Battle Bunker"/>
            <img width="60" height="48" src="assets/upgrades/Helix_Speaker_Tower.webp" alt="Speaker Tower"/>
            <img width="60" height="48" src="assets/upgrades/Subliminal_Messaging.webp" alt="Subliminal Messaging"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            7.500$
          </td>
          <td style="text-align: left">
            + 8 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>1 Sniper + 7 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 25.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Airfield</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Air Support

<table>
  <tr>
    <th>MiG 1.44 - Black Napalm</th>
    <th>MiG 1.44 - Tactical Nuke</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/MiG.webp" alt="MiG"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/MiG_Armor.webp" alt="MiG Armor"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
            <img width="60" height="48" src="assets/upgrades/Black_Napalm.webp" alt="Black Napalm"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.250$
          </td>
          <td style="text-align: left">
            + Devastating Firestorm
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/MiG.webp" alt="MiG"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/MiG_Armor.webp" alt="MiG Armor"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
            <img width="60" height="48" src="assets/upgrades/Tactical_Nuke_MiG.webp" alt="Tactical Nuke MiG"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 15.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Airfield</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Stealth Bomber

<table>
<tr>
  <th>F-117 Nighthawk</th>
  <th>B-2 Spirit</th>
</tr>
<tr>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/Stealth_Fighter.webp" alt="Stealth Fighter"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <img width="60" height="48" src="assets/upgrades/Laser-Guided_Missiles.webp" alt="Laser-Guided Missiles"/>
          <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
          <img width="60" height="48" src="assets/upgrades/Bunker_Busters.webp" alt="Bunker Busters"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          2.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser<br>
          + Stealth Capability
        </td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/MOAB.webp" alt="MOAB"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <img width="60" height="48" src="assets/upgrades/Laser-Guided_Missiles.webp" alt="Laser-Guided Missiles"/>
          <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
          <img width="60" height="48" src="assets/upgrades/Nuclear_Missile.webp" alt="Nuclear Missile"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          5.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser<br>
          + Stealth Capability<br>
          + Devastating Nuclear Missile
        </td>
      </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 25.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Airfield</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Air Superiority

<table>
<tr>
  <th>F-22 Raptor</th>
  <th>F-22X King Raptor</th>
</tr>
<tr>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/Raptor.webp" alt="Raptor"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <img width="60" height="48" src="assets/upgrades/Laser-Guided_Missiles.webp" alt="Laser-Guided Missiles"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          2.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser
        </td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/King_Raptor.webp" alt="King Raptor"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <img width="60" height="48" src="assets/upgrades/Laser-Guided_Missiles.webp" alt="Laser-Guided Missiles"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
          <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          3.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser<br>
          + Stealth Capability
        </td>
      </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 15.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Airfield</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Supersonic Bomber

<table>
<tr>
  <th>F-16XL Aurora</th>
  <th>SR-72 Aurora Alpha</th>
</tr>
<tr>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/Aurora_Alpha.webp" alt="Aurora Alpha"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          3.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser<br>
          + Fuel Air Bomb
        </td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="text-align: center">
          <img width="122" height="98" src="assets/units/Hypersonic_Aurora_Alpha.webp" alt="Hypersonic Aurora Alpha"/>
        </td>
        <td style="text-align: left">
          <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
          <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          <br>
          <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
        </td>
      </tr>
      <tr>
        <td style="text-align: center">
          5.000$
        </td>
        <td style="text-align: left">
          + Point Defense Laser<br>
          + MOAB<br>
          + Stealth Capability
        </td>
      </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
    <ul>
      <li>Requires Research at the <i>Research Center</i></li>
      <li>Research Cost: 25.000$</li>
      <li>Requires <i>Strategy Center</i></li>
      <li>Produced at the <i>Airfield</i></li>
    </ul>
    </td>
  </tr>
</table>

#### Special Units

<table>
  <tr>
    <th>
      A-10 Thunderbolt II
    </th>
    <th>
      <img src="assets/star.png" alt="Hero"/> AC-130H Gunship
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/A-10.webp" alt="A-10 Thunderbolt II"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
            <img width="60" height="48" src="assets/upgrades/Laser-Guided_Missiles.webp" alt="Laser-Guided Missiles"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            7.500$
          </td>
          <td style="text-align: left">
            + Auto-Repair<br>
            + Reloads Mid-Air<br>
            + 12 Anti-Tank Missiles
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Spectre_Gunship.webp" alt="Spectre Gunship"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Countermeasures.webp" alt="Countermeasures"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            50.000$
          </td>
          <td style="text-align: left">
            + Flying Fortress<br>
            + Point Defense Laser<br>
            + EMP-Hardened<br>
            + Howitzer<br>
            + Gatling Cannon
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Unique Unit</li>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 100.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Airfield</i></li>
      </ul>
    </td>
  </tr>
</table>