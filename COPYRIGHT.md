All images in this repository were taken from
the [Command & Conquer: Generals - Zero Hour](https://cnc.fandom.com/wiki/Command_%26_Conquer:_Generals_-_Zero_Hour),
the manuals, websites, videos, and/or other promotional material created and owned by Electronic Arts or any Command &
Conquer development studio, who hold the copyright of Command & Conquer. All trademarks and registered trademarks
present in the images are proprietary to Electronic Arts. For more information, see
the [copyright notice](https://cnc.fandom.com/wiki/Command_and_Conquer_Wiki:Copyrights). The use of images to illustrate
articles concerning the subject of the images in question is believed to qualify as fair use under United States
copyright law, as such display does not significantly impede the right of the copyright holder to sell the copyrighted
material.