![USA Air Force General](assets/CNCGZH_Logo.webp)

# Super General - Europe

---

### Base Buildings

##### Base Building Dozer

---

- Advance Cold Fusion Reactor ![USA Superweapons General](assets/A_super.webp)
    - Advanced Control Rods (+300% Energy)
    - 2.000$
- Command Center ![USA](assets/Gen_USA_Logo.webp)
    - Neutron Mines
    - 2.000$
- Supply Center ![USA](assets/Gen_USA_Logo.webp)
    - Supply Lines (+10% Income)
    - Requires *Cold Fusion Reactor*
    - 1.000$
- Speaker Tower ![China](assets/Gen_China_Logo.webp)
    - Subliminal Messaging (+25% Propaganda Buff)
    - Neutron Mines
    - Requires *Cold Fusion Reactor*
    - 1.500$

---

- Barracks ![USA](assets/Gen_USA_Logo.webp)
    - 1.000$
- War Factory ![USA](assets/Gen_USA_Logo.webp)
    - Requires *Supply Center*
    - 3.000$
- Airfield ![USA Air Force General](assets/A_air.webp)
    - Requires *Supply Center*
    - 3.000$
- Strategy Center ![USA](assets/Gen_USA_Logo.webp)
    - Spy Drone
    - Spy Satellite
    - Intelligence
    - Requires both *War Factory* **and** *Airfield*
    - 5.000$

---

- Web Center ![China Infantry General](assets/C_inf.webp)
    - 12 Super Hackers ![Heroic Rank](assets/hero.png)
    - Neutron Mines
    - Requires *Strategy Center*
    - 10.000$
- Cruise Missile Launcher ![USA Superweapons General](assets/A_super.webp)
    - Requires *Strategy Center*
    - 5.000$
- Nuclear Storm Launcher ![China Nuclear General](assets/C_nuke.webp)
    - Nuclear Warheads
    - Neutron Mines
    - Camo Netting
    - Requires *Strategy Center*
    - 10.000$

### Base Defense

##### Base Defense Dozer

- Fortified Bunker ![China Infantry General](assets/C_inf.webp)
    - Neutron Mines
    - 1.000$
- Palace ![GLA](assets/Gen_GLA_Logo.webp)
    - Fortified Structure
    - 14 Infantry Slots
    - 3.000$
- Tunnel Network ![GLA Stealth General](assets/G_stlth.webp)
    - Camo Netting
    - 1.000$
- Firebase ![WA](assets/Gen_USA_Logo.webp)
    - 1.000$

---

- Patriot Missile System ![USA](assets/Gen_USA_Logo.webp)
    - Requires *Cold Fusion Reactor*
    - 1.000$
- EMP Patriot System ![USA Superweapons General](assets/A_super.webp)
    - Requires *Cold Fusion Reactor*
    - 1.250$
- Laser Turret ![USA Laser General](assets/A_laser.webp)
    - Requires *Cold Fusion Reactor*
    - 1.250$
- Advanced Patriot System ![GLA Toxin General](assets/G_toxin.webp)
    - +100% Damage
    - Requires *Cold Fusion Reactor*
    - 2.000$

---

- Speaker Tower ![China](assets/Gen_China_Logo.webp)
    - Subliminal Messaging (+25% Propaganda Buff)
    - Neutron Mines
    - Requires *Cold Fusion Reactor*
    - 1.500$

---

### Units

---

##### Infantry

---

- Mini-Gunner ![China Infantry General](assets/C_inf.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - Patriotism (+25% Fire Rate)
    - Capture Building
    - 500$
- Missile Defender ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - 500$
- Ranger ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - Capture Building
    - Machine Gun
    - Missile Launcher
    - Flash-Bang Grenades
    - Laser Guided Missiles
    - 1.000$
- Sniper ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - 1.000$
- Hijacker ![GLA Stealth General](assets/G_stlth.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - Camouflaged
    - 1.000$

---

- Agent
  47 ![GLA Demolition General](assets/G_demo.webp) ![USA](assets/Gen_USA_Logo.webp) ![China Infantry General](assets/C_inf.webp)
    - Hero Unit
    - Advanced Training (+100% XP)
    - Chemical Suits
    - Armor-Piercing Bullets (+25% Damage)
    - Sniper Attack
    - Knife Attack
    - Remote Demo Charge
    - Detonate Charges
    - Capture Building
    - Disable Vehicle
    - Cash Hack
    - Disable Building
    - Demolition
    - Requires *Strategy Center*
    - 2.000$
- Pilot ![Heroic Rank](assets/hero.png) ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - Chemical Suits
    - Requires *Strategy Center*
    - 3.000$

---

- Super Hacker ![Heroic Rank](assets/hero.png) ![China Infantry General](assets/C_inf.webp)
    - Only in *Web Center*

---

##### Vehicles

---

- Dozer ![USA](assets/Gen_USA_Logo.webp)
    - 500$

---

- Mechanic ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - 3 Infantry Slots (1 Missile Defender)
    - Repairs Vehicles
    - Heals Infantry
    - 1.000$
- Humvee ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - TOW Missile
    - 5 Infantry Slots (1 Sniper + 2 Missile Defender)
    - 1.500$
- Attack Outpost ![China Infantry General](assets/C_inf.webp)
    - Advanced Training (+100% XP)
    - Composite Armor (+20% Armor)
    - Subliminal Messaging (+25% Propaganda Buff)
- Quad Cannon ![GLA Stealth General](assets/G_stlth.webp)
    - Junk Repair (+2HP/s)
    - Fully Salvage-Upgraded
    - Armor-Piercing Bullets (+25% Damage)
    - Snipe Rounds
    - Camouflaged
    - 1.000$
- Avenger ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - 2.000$
- Paladin ![USA](assets/Gen_USA_Logo.webp)
    - Advanced Training (+100% XP)
    - Composite Armor (+20% Armor)
    - 1.500$
- Laser Tank ![USA Laser General](assets/A_laser.webp)
    - Advanced Training (+100% XP)
    - Composite Armor (+20% Armor)
    - 1.000$

---

- SCUD Launcher ![GLA Toxin General](assets/G_toxin.webp)
    - Junk Repair (+2HP/s)
    - Armor-Piercing Rockets (+25% Damage)
    - Anthrax Gamma (+25% Damage)
    - Fully Salvage-Upgraded
    - High-Explosive Warheads
    - Anthrax Gamma Warheads
    - 1.500$
- Rocket Buggy ![GLA Demolition General](assets/G_demo.webp)
    - Junk Repair (+2HP/s)
    - Armor-Piercing Rockets (+25% Damage)
    - Buggy Ammo (+100% Ammo)
    - Demolition
    - 1.500$
- Nuke Cannon ![China Nuclear General](assets/C_nuke.webp)
    - Neutron Shells
    - 2.000$

---

- Battlemaster ![China Nuclear General](assets/C_nuke.webp)
    - Uranium Shells (+25% Damage)
    - Nuclear Tanks (+25% Movement Speed)
    - Patriotism (+25% Fire Rate)
    - Autoloader (+10% Fire Rate)
    - Isotope Stability
    - Requires *Strategy Center*
    - 1.250$
- **Emperor Overlord** ![Heroic Rank](assets/hero.png) ![China Tank General](assets/C_tank.webp)
    - Uranium Shells (+25% Damage)
    - Nuclear Tanks (+25% Movement Speed)
    - Battle Bunker (1 Sniper + 3 Gattling-Gunner + 4 Missile Defender)
    - Propaganda Tower (2% HP/s Repair Aura)
    - Subliminal Messaging (+25% Propaganda Buff)
    - Isotope Stability
    - Requires *Strategy Center*
    - 25.000$

---

##### Aircraft

---

- Chinook ![USA](assets/Gen_USA_Logo.webp)
    - Point-Defense Laser
    - Countermeasures
    - Supply Lines (+10% Income)
    - 1.500$
- Combat Chinook ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Countermeasures
    - Supply Lines (+10% Income)
    - 10 Slots
    - 2.000$
- Stealth Comanche ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Advanced Training (+100% XP)
    - Countermeasures
    - Rocket Pods
    - 2.000$
- Assault Helix ![China Nuclear General](assets/C_nuke.webp)
    - Speaker Tower
    - Gattling Cannon
    - Battle Bunker (6 Infantry Slots)
    - Black Napalm Bomb
    - Nuclear Bomb
    - Requires *Strategy Center*
    - 3.500$

---

- Stealth Fighter F-117 ![USA](assets/Gen_USA_Logo.webp)
    - Point-Defense Laser
    - Advanced Training (+100% XP)
    - Countermeasures
    - Laser-Guided Missiles (+25% Damage)
    - Bunker Busters
    - 2.000$
- Stealth MiG 1.44 ![China](assets/Gen_China_Logo.webp)
    - Point-Defense Laser
    - Countermeasures
    - MiG Armor (+25% Armor)
    - Black Napalm
    - 2.500$
- Stealth Nuke MiG 1.44 ![China Nuclear General](assets/C_nuke.webp)
    - Point-Defense Laser
    - Countermeasures
    - MiG Armor (+25% Armor)
    - Tactical Nuke
    - Requires *Strategy Center*
    - 2.500$
- Stealth King Raptor F-22 ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Advanced Training (+100% XP)
    - Countermeasures
    - Laser-Guided Missiles (+25% Damage)
    - Requires *Strategy Center*
    - 2.500$
- **Stealth Aurora Alpha SR-72
  ** ![Heroic Rank](assets/hero.png) ![USA Superweapons General](assets/A_super.webp) ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Advanced Training (+100% XP)
    - Countermeasures
    - Requires *Strategy Center*
    - 3.500$
- **Stealth Bomber B-2 Spirit** ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Advanced Training (+100% XP)
    - Countermeasures
    - Laser-Guided Missiles (+25% Damage)
    - Bunker Busters
    - 5.000$
- **Hercules AC-130 Spectre Gunship** ![USA Air Force General](assets/A_air.webp)
    - Hero Unit
    - Requires *Strategy Center*
    - 50.000$

---

##### Special Units

---

- Stealth Bomber B-3 ![USA Air Force General](assets/A_air.webp)
    - Point-Defense Laser
    - Countermeasures

---

### General Powers

---

##### Rank 1

###### *1 Point*

---

- Sneak Attack ![GLA](assets/Gen_GLA_Logo.webp)
- Cash Bounty III ![GLA](assets/Gen_GLA_Logo.webp)
    - 20% Unit Cost Reward
- Anthrax Bomb ![GLA Toxin General](assets/G_toxin.webp)
    - Anthrax Gamma (+25% Damage)

---

##### Rank 3

###### *+2 Points*

---

- MOAB ![USA Air Force General](assets/A_air.webp)
    - 1x *B-3 Stealth Bomber* delivering the *Mother Of All Bombs* (or *Massive Ordnance Air Blast*)
- Artillery Barrage III ![China](assets/Gen_China_Logo.webp)
- Spectre Gunship III ![USA Superweapons General](assets/A_super.webp)
- A-10 Thunderbolt Missile Strike III ![USA](assets/Gen_USA_Logo.webp)

---

##### Rank 5

###### *+4 Points*

---

- Carpet Bomb ![USA Air Force General](assets/A_air.webp)
    - 1x *B-3 Stealth Bomber* delivering 12 Bombs
- Nuclear Carpet Bomb ![China Nuclear General](assets/C_nuke.webp)
    - 1x *B-3 Stealth Bomber* delivering 12 Tactical Nukes
- **Super Carpet Bomb** ![Ultimate Ability](assets/star.png) ![USA Air Force General](assets/A_air.webp) ![China Nuclear General](assets/C_nuke.webp)
    - 5x *B-3 Stealth Bomber* in V-Formation delivering 12 Tactical Nukes each
    - Ordered from the Nuclear Storm Launcher
    - Requires both *Carpet Bomb* **and** *Nuclear Carpet Bomb*
    - 10min Cooldown

---

### Superweapons

---

- Cruise Missile
    - 2min Cooldown
- **Nuclear Storm** ![Ultimate Ability](assets/star.png) ![GLA Stealth General](assets/G_stlth.webp) ![China Nuclear General](assets/C_nuke.webp)
    - Fires 9 Nuclear Missiles
    - 10min Cooldown

---

##### Notes

- No AI (AI in Skirmish will do nothing, TODO: make non-selectable for *Random AI*)
- `-noshellmap -nologo -quickstart`