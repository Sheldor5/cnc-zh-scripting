# Vehicles

#### Command Center

<table style="text-align: center">
  <tr>
    <td>
      Base Dozer<br><img width="122" height="98" src="assets/units/USA_Construction_Dozer.webp" alt="Base Dozer"/>
    </td>
    <td>
      Defense Dozer<br><img width="122" height="98" src="assets/units/China_Construction_Dozer.webp" alt="Defense Dozer"/>
    </td>
  </tr>
  <tr>
    <td>
      Armoured Attack-APC<br><img width="122" height="98" src="assets/units/USA_POW_Truck.webp" alt="Armoured Attack-APC"/>
    </td>
    <td>
      Nuke Truck<br><img width="122" height="98" src="assets/units/Nuke_Truck.webp" alt="Nuke Truck"/>
    </td>
  </tr>
</table>

#### War Factory

<table style="text-align: center">
  <tr>
    <td>
       Mechanic<br><img width="122" height="98" src="assets/units/Mechanic.webp" alt="Mechanic"/>
    </td>
    <td>
      Humvee<br><img width="122" height="98" src="assets/units/Humvee.webp" alt="Humvee"/>
    </td>
    <td>
      Laser Tank<br><img width="122" height="98" src="assets/units/Laser_Tank.webp" alt="Laser Tank"/>
    </td>
    <td>
      Avenger<br><img width="122" height="98" src="assets/units/Avenger.webp" alt="Avenger"/>
    </td>
    <td>
      Laser Paladin<br><img width="122" height="98" src="assets/units/Paladin.webp" alt="Paladin"/>
    </td>
    <td>
      Overlord<br><img width="122" height="98" src="assets/units/Overlord_Tank.webp" alt="Overlord"/>
    </td>
  </tr>
  <tr>
    <td>
      Attack Outpost<br><img width="122" height="98" src="assets/units/Listening_Outpost.webp" alt="Attack Outpost"/>
    </td>
    <td>
      Quad Cannon<br><img width="122" height="98" src="assets/units/Quad_Cannon.webp" alt="Quad Cannon"/>
    </td>
    <td>
      Rocket Buggy<br><img width="122" height="98" src="assets/units/Rocket_Buggy.webp" alt="Rocket Buggy"/>
    </td>
    <td>
      SCUD Launcher<br><img width="122" height="98" src="assets/units/Scud_Launcher.webp" alt="SCUD Launcher"/>
    </td>
    <td>
      Nuke Cannon<br><img width="122" height="98" src="assets/units/Nuke_Cannon.webp" alt="Nuke Cannon"/>
    </td>
    <td>
      <img src="assets/star.png" alt="Hero"/> Emperor<br><img width="122" height="98" src="assets/units/Emperor_Overlord.webp" alt="Emperor Overlord"/>
    </td>
  </tr>
</table>

### Fact Sheets

#### Construction

<table>
  <tr>
    <th>
      Base Dozer
    </th>
    <th>
      Defense Dozer
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/USA_Construction_Dozer.webp" alt="Base Dozer"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite_Armor"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            500$
          </td>
          <td style="text-align: left">
            + Auto-Repair
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/China_Construction_Dozer.webp" alt="Defense Dozer"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
            <img width="60" height="48" src="assets/upgrades/Clear_Mines.webp" alt="Clear Mines"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            500$
          </td>
          <td style="text-align: left">
            + Auto-Repair
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Command Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>Command Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Support

<table>
  <tr>
    <th>
      Mechanic
    </th>
    <th>
      Super Drone
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Mechanic.webp" alt="Mechanic"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Cleanup.webp" alt="Cleanup"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite_Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.500$
          </td>
          <td style="text-align: left">
            + Repairs nearby Vehicles<br>
            + Heals nearby Soldiers<br>
            + 3 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>1 Sniper + 2 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Super_Drone.png" alt="Super Drone"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Sentry_Drone_Gun.webp" alt="Sentry Drone Gun"/>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Drone_Armor.webp" alt="Drone Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            + Mini-Gun<br>
            + Point Defense Laser<br>
            + Stealth Detector<br>
            + Auto-Repair<br>
            + Repairs Owner
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Available for:</li>
        <ul>
          <li>Mechanic</li>
          <li>Humvee</li>
          <li>Laser Paladin</li>
          <li>Avenger</li>
        </ul>
      </ul>
    </td>
  </tr>
</table>

#### Transport

<table>
  <tr>
    <th>
      Humvee
    </th>
    <th>
      Attack Outpost
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Humvee.webp" alt="Humvee"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Chainguns.webp" alt="Chainguns"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
            <img width="60" height="48" src="assets/upgrades/TOW_Missile.webp" alt="TOW Missile"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            + 5 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>1 Mini-Gunner + 2 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Listening_Outpost.webp" alt="Attack Outpost"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Subliminal_Messaging.webp" alt="Subliminal Messaging"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.250$
          </td>
          <td style="text-align: left">
            + 8 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>1 Mini-Gunner + 3 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Anti-Air

<table>
  <tr>
    <th>
      Quad Cannon
    </th>
    <th>
      Avenger
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Quad_Cannon.webp" alt="Quad Cannon"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Junk_Repair.webp" alt="Junk Repair"/>
            <img width="60" height="48" src="assets/upgrades/Quad_Cannon_Snipe_Rounds.webp" alt="Quad Cannon Snipe Rounds"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/AP_Bullets.webp" alt="Composite_Armor"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.250$
          </td>
          <td style="text-align: left">
            + Fully Salvage-Upgraded
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Avenger.webp" alt="Avenger"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            + 2x Point-Defense Laser
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Tanks

<table>
  <tr>
    <th>
      Laser Tank
    </th>
    <th>
      Laser Paladin
    </th>
    <th>
      Overlord
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Laser_Tank.webp" alt="Laser Tank"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite_Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-1 Energy</sup>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Paladin.webp" alt="Laser Paladin"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite_Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$<br><sup>-2 Energy</sup>
          </td>
          <td style="text-align: left">
            + Point-Defense Laser<br>
            + Laser Cannon
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Overlord_Tank.webp" alt="Overlord"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Uranium_Shells.webp" alt="Uranium Shells"/>
            <img width="60" height="48" src="assets/upgrades/Isotope_Stability.webp" alt="Isotope Stability"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite_Armor"/>
            <img width="60" height="48" src="assets/upgrades/Nuclear_Tanks.webp" alt="Nuclear Tanks"/>
            <img width="60" height="48" src="assets/upgrades/Nationalism.webp" alt="Nationalism"/>
            <br>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.500$
          </td>
          <td style="text-align: left">
            + Horde Bonus
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Artillery

<table>
  <tr>
    <th>
      Rocket Buggy
    </th>
    <th>
      SCUD Launcher
    </th>
    <th>
      Nuke Cannon
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Rocket_Buggy.webp" alt="Rocket Buggy"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Junk_Repair.webp" alt="Junk Repair"/>
            <img width="60" height="48" src="assets/upgrades/Rocket_Buggy_Ammo.webp" alt="Rocket Buggy Ammo"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/AP_Rockets.webp" alt="Junk Repair"/>
            <img width="60" height="48" src="assets/upgrades/Anthrax_Gamma.webp" alt="Anthrax Gamma"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.500$
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Scud_Launcher.webp" alt="SCUD Launcher"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Junk_Repair.webp" alt="Junk Repair"/>
            <img width="60" height="48" src="assets/upgrades/High_Explosive_Warhead.webp" alt="High Explosive Warhead"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/AP_Rockets.webp" alt="AP Rockets"/>
            <img width="60" height="48" src="assets/upgrades/Anthrax_Gamma_Warhead.webp" alt="Anthrax Gamma Warhead"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            + Fully Salvage-Upgraded
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Nuke_Cannon.webp" alt="Nuke Cannon"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Nuclear_Shells.webp" alt="Nuclear Shells"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Neutron_Shells.webp" alt="Neutron Shells"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 5.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 5.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Special Units

<table>
  <tr>
    <th>
      Nuke Truck
    </th>
    <th>
      Armoured Attack-APC
    </th>
    <th>
      <img src="assets/star.png" alt="Hero"/> Emperor
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Nuke_Truck.webp" alt="Nuke Truck"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Nuclear_Missile.webp" alt="Detonate Nuclear Load"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.500$
          </td>
          <td style="text-align: left">
            + Devastating Nuclear Load<br>
            + Auto-Repair
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/USA_POW_Truck.webp" alt="Armoured Attack-APC"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Emergency_Repair_1.webp" alt="Auto-Repair"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Composite_Armor.webp" alt="Composite Armor"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            5.000$
          </td>
          <td style="text-align: left">
            + Auto-Repair<br>
            + 10 Gunports<br>
            &nbsp;&nbsp;&nbsp;<sup>4 Mini-Gunner + 6 Missile Defender</sup>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Emperor_Overlord.webp" alt="Emperor"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Nuclear_Tanks.webp" alt="Nuclear Tanks"/>
            <img width="60" height="48" src="assets/upgrades/Autoloader.webp" alt="Autoloader"/>
            <img width="60" height="48" src="assets/upgrades/Overlord_Propaganda_Tower.webp" alt="Overlord Propaganda Tower"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Uranium_Shells.webp" alt="Uranium Shells"/>
            <img width="60" height="48" src="assets/upgrades/Overlord_Gatling_Cannon.webp" alt="Overlord Gatling Cannon"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            25.000$
          </td>
          <td style="text-align: left">
            + Moving Fortress
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 10.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Command Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Command Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Unique Unit</li>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 50.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>War Factory</i></li>
      </ul>
    </td>
  </tr>
</table>