# Buildings

### Base Dozer <img style="margin-bottom: -3px" src="assets/Gen_USA_Logo.webp"/>

<table style="text-align: center">
  <tr>
    <td>
      Super Reactor<br><img width="122" height="98" src="assets/buildings/Advanced_Cold_Fusion_Reactor.webp" alt="Super Reactor"/>
    </td>
    <td>
      Barracks<br><img width="122" height="98" src="assets/buildings/Barracks.webp" alt="Barracks"/>
    </td>
    <td>
      <div style="width: 122px"></div>
    </td>
    <td>
      War Factory<br><img width="122" height="98" src="assets/buildings/War_Factory.webp" alt="War Factory"/>
    </td>
    <td>
      Airfield<br><img width="122" height="98" src="assets/buildings/Airfield.webp" alt="Airfield"/>
    </td>
    <td>
      Research Center<br><img width="122" height="98" src="assets/buildings/Propaganda_Center.webp" alt="Research Center"/>
    </td>
    <td>
      Cruise Missile Silo<br><img width="122" height="98" src="assets/buildings/Cruise_Missile_Silo.webp" alt="Cruise Missile Silo"/>
    </td>
  </tr>
  <tr>
    <td>
      Command Center<br><img width="122" height="98" src="assets/buildings/Command_Center.webp" alt="Command Center"/>
    </td>
    <td>
      Supply Center<br><img width="122" height="98" src="assets/buildings/Supply_Center.webp" alt="Supply Center"/>
    </td>
    <td>
      <div style="width: 122px"></div>
    </td>
    <td>
      Speaker Tower<br><img width="122" height="98" src="assets/buildings/Speaker_Tower.webp" alt="Speaker Tower"/>
    </td>
    <td>
      Strategy Center<br><img width="122" height="98" src="assets/buildings/Strategy_Center.webp" alt="Strategy Center"/>
    </td>
    <td>
      Web Center<br><img width="122" height="98" src="assets/buildings/Internet_Center.webp" alt="Web Center"/>
    </td>
    <td>
      Nuclear Storm<br><img width="122" height="98" src="assets/buildings/Tomahawk_Storm.webp" alt="Nuclear Storm"/>
    </td>
  </tr>
</table>

### Defense Dozer <img style="margin-bottom: -3px" src="assets/Gen_China_Logo.webp"/>

<table style="text-align: center">
  <tr>
    <td>
       Fortified Bunker<br><img width="122" height="98" src="assets/buildings/Fortified_Bunker.webp" alt="Fortified Bunker"/>
    </td>
    <td>
       Firebase<br><img width="122" height="98" src="assets/buildings/Fire_Base.webp" alt="Firebase"/>
    </td>
    <td>
      <div style="width: 122px"></div>
    </td>
    <td>
       Patriot System<br><img width="122" height="98" src="assets/buildings/Patriot.webp" alt="Patriot System"/>
    </td>
    <td>
       EMP Patriot<br><img width="122" height="98" src="assets/buildings/EMP_Patriot.webp" alt="EMP Patriot"/>
    </td>
    <td>
       Advanced Patriot System<br><img width="122" height="98" src="assets/buildings/Patriot2.png" alt="Advanced Patriot System"/>
    </td>
  </tr>
  <tr>
    <td>
      Fortified Palace<br><img width="122" height="98" src="assets/buildings/Palace.webp" alt="Fortified Palace"/>
    </td>
    <td>
      Toxin Network<br><img width="122" height="98" src="assets/buildings/Toxin_Network.webp" alt="Toxin Network"/>
    </td>
    <td>
      <div style="width: 122px"></div>
    </td>
    <td>
      Speaker Tower<br><img width="122" height="98" src="assets/buildings/Speaker_Tower.webp" alt="Speaker Tower"/>
    </td>
    <td>
      Laser Turret<br><img width="122" height="98" src="assets/buildings/Laser_Defense_Turret.webp" alt="Laser Turret"/>
    </td>
    <td>
      Advanced Laser Turret<br><img width="122" height="98" src="assets/buildings/Laser_Defense_Turret.webp" alt="Advanced Laser Turret"/>
    </td>
  </tr>
</table>

- *Advanced* defense structures deal 250% Damage

### Fact Sheets

#### Basic Structures

<table>
  <tr>
    <th>Command Center</th>
    <th>Super Reactor</th>
    <th>Supply Center</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Command_Center.webp" alt="Command Center"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Spy_Drone.webp" alt="Spy Drone"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Neutron_Mines.webp" alt="Neutron Mines"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            <ul>
              <li>Spy Drone</li>
              <li>General's Powers:</li>
              <ul>
                <li>EMP</li>
                <li>Sneak Attack</li>
                <li>Paradrop</li>
                <li>Tank Drop</li>
                <li>Anthrax Bomb</li>
              </ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Advanced_Cold_Fusion_Reactor.webp" alt="Super Reactor"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Control_Rods.webp" alt="Advanced Control Rods"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Control_Rods.webp" alt="Control Rods"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$<br><sup>+32 Energy</sup>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Supply_Center.webp" alt="Supply Center"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Supply_Lines.webp" alt="Supply Lines"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-2 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
      </ul>
    </td>
    <td>
      <ul>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Super Reactor</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Garrison Defense Structures

<table>
  <tr>
    <th>Fortified Bunker</th>
    <th>Firebase</th>
    <th>Fortified Palace</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Fortified_Bunker.webp" alt="Fortified Bunker"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Neutron_Mines.webp" alt="Neutron Mines"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Fortified_Structure.webp" alt="Fortified Structure"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            <ul>
              <li>10 Gunports</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Fire_Base.webp" alt="Firebase"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            <ul>
              <li>4 Gunports</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Palace.webp" alt="Fortified Palace"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Fortified_Structure.webp" alt="Fortified Structure"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            3.000$
          </td>
          <td style="text-align: left">
            <ul>
              <li>14 Gunports</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

#### Production Facilities

<table>
  <tr>
    <th>Barracks</th>
    <th>War Factory</th>
    <th>Airfield</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Barracks.webp" alt="Barracks"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/War_Factory.webp" alt="War Factory"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$<br><sup>-6 Energy</sup>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Airfield.webp" alt="Airfield"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-4 Energy</sup>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Supply Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Supply Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Support Systems

<table>
  <tr>
    <th>Toxin Network</th>
    <th>Speaker Tower</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Toxin_Network.webp" alt="Toxin Network"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Anthrax_Gamma.webp" alt="Anthrax Gamma"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Camo_Netting.webp" alt="Camo Netting"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            <ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Speaker_Tower.webp" alt="Speaker Tower"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Subliminal_Messaging.webp" alt="Subliminal Messaging"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-2 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>High Range</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Super Reactor</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Automatic Defense Systems

<table>
  <tr>
    <th>Patriot System</th>
    <th>Laser Turret</th>
    <th>EMP Patriot</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Patriot.webp" alt="Patriot System"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-3 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Laser_Defense_Turret.webp" alt="Laser Turret"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$<br><sup>-5 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/EMP_Patriot.webp" alt="EMP Patriot"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.500$<br><sup>-5 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires <i>Super Reactor</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Super Reactor</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Hi-Tech Buildings

<table>
  <tr>
    <th>Strategy Center</th>
    <th>Research Center</th>
    <th>Web Center</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Strategy_Center.webp" alt="Strategy Center"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Spy_Sattelite.webp" alt="Spy Satellite"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Intelligence.webp" alt="CIA Intelligence"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            5.000$<br><sup>-5 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>Deploy Battle Plans</li>
              <li>Spy Satellite Scan</li>
              <li>CIA Intelligence</li>
              <li>General's Powers:</li>
              <ul>
                <li>Carpet Bomb</li>
                <li>Fuel Air Bomb</li>
                <li>MOAB</li>
                <li>Artillery Barrage</li>
                <li>A-10 Strike</li>
                <li>Napalm Strike</li>
              </ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Propaganda_Center.webp" alt="Research Center"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Neutron_Mines.webp" alt="Neutron Mines"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            10.000$<br><sup>-10 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>Research Units</li>
              <li>500$/10s Patent Profits</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Internet_Center.webp" alt="Web Center"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Satellite_Hack_1.webp" alt="Satellite Hack 1"/>
            <img width="60" height="48" src="assets/upgrades/Neutron_Mines.webp" alt="Neutron Mines"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Satellite_Hack_2.webp" alt="Satellite Hack 2"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            25.000$<br><sup>-20 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>50$/s Crypto Mining</li>
              <li>Employs 10 Super-Hackers<br><sup>2$ / 4$ / 7$ / 10$</sup></li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires <i>War Factory</i> and <i>Airfield</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Advanced Defense Systems

<table>
  <tr>
    <th>Advanced Patriot System</th>
    <th>Advanced Laser Turret</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Patriot2.png" alt="Advanced Patriot System"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.500$<br><sup>-5 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>300% Damage</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Laser_Defense_Turret.webp" alt="Laser Turret"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.500$<br><sup>-8 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>300% Damage</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Superweapons

<table>
  <tr>
    <th>Cruise Missile Silo</th>
    <th>Nuclear Storm</th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Cruise_Missile_Silo.webp" alt="Cruise Missile Silo"/>
          </td>
          <td style="text-align: left">
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            5.000$<br><sup>-5 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>ICB-Missile</li>
              <li>4:00 Cooldown</li>
            </ul>
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/buildings/Tomahawk_Storm.webp" alt="Nuclear Storm"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Nuclear_Missile.webp" alt="Fire Nuclear Storm"/>
            <img width="60" height="48" src="assets/upgrades/Neutron_Mines.webp" alt="Neutron Mines"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Camo_Netting.webp" alt="Camo Netting"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            100.000$<br><sup>-10 Energy</sup>
          </td>
          <td style="text-align: left">
            <ul>
              <li>9 Nuclear Missiles</li>
              <li>10:00 Cooldown</li>
              <li>General's Powers:</li>
              <ul>
                <li>Super Carpet Bomb</li>
              </ul>
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Cruise Missile Silo</i></li>
      </ul>
    </td>
  </tr>
</table>