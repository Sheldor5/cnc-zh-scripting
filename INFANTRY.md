# Infantry

#### Barracks

<table style="text-align: center">
  <tr>
    <td>
       Mini-Gunner<br><img width="122" height="98" src="assets/units/Ranger.webp" alt="Mini-Gunner"/>
    </td>
    <td>
      Missile-Defender<br><img width="122" height="98" src="assets/units/Missile_Defender.webp" alt="Missile-Defender"/>
    </td>
    <td>
      Sniper<br><img width="122" height="98" src="assets/units/Pathfinder.webp" alt="Sniper"/>
    </td>
    <td>
      <div style="width: 116px"></div>
    </td>
    <td>
      Nuke Hunter<br><img width="122" height="98" src="assets/units/NukeHunter.png" alt="Nuke Hunter"/>
    </td>
  </tr>
  <tr>
    <td>
      Saboteur<br><img width="122" height="98" src="assets/units/Saboteur.webp" alt="Saboteur"/>
    </td>
    <td>
      Hijacker<br><img width="122" height="98" src="assets/units/Hijacker.webp" alt="Hijacker"/>
    </td>
    <td>
      <img src="assets/hero.png" alt="Hero"/> Pilot<br><img width="122" height="98" src="assets/units/Pilot.webp" alt="Pilot"/>
    </td>
    <td>
      <div style="width: 116px"></div>
    </td>
    <td>
      <img src="assets/star.png" alt="Hero"/> Agent 47<br><img width="122" height="98" src="assets/units/Jarmen_Kell.webp" alt="Agent 47"/>
    </td>
  </tr>
</table>

### Fact Sheets

#### Combat

<table>
  <tr>
    <th>
      Mini-Gunner
    </th>
    <th>
      Missile-Defender
    </th>
    <th>
      Sniper
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Ranger.webp" alt="Mini-Gunner"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Chainguns.webp" alt="Chainguns"/>
            <img width="60" height="48" src="assets/upgrades/Capture_Building.webp" alt="Capture Building"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/Flashbangs.webp" alt="Flashbangs"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            750$
          </td>
          <td style="text-align: left">
            + Horde Bonus
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Missile_Defender.webp" alt="Missile-Defender"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Laser_Lock.webp" alt="Laser Lock"/>
            <img width="60" height="48" src="assets/upgrades/Anthrax_Gamma.webp" alt="Anthrax Gamma"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/AP_Rockets.webp" alt="AP Rockets"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            750$
          </td>
          <td style="text-align: left">
            + Horde Bonus
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Pathfinder.webp" alt="Sniper"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/AP_Bullets.webp" alt="Snipe Rounds"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            + Horde Bonus
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Agents

<table>
  <tr>
    <th>
      Saboteur
    </th>
    <th>
      Hijacker
    </th>
    <th>
      Super Hacker
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Saboteur.webp" alt="Saboteur"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            500$
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Hijacker.webp" alt="Hijacker"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            500$
          </td>
          <td style="text-align: left">
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Hacker.webp" alt="Hacker"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Hack_Internet.webp" alt="Hack Internet"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            - $
          </td>
          <td style="text-align: left">
            Income: 2$ / 4$ / 7$ / 10$
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Only available in the <i>Web Center</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Experiments

<table>
  <tr>
    <th>
      Nuke Hunter
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/NukeHunter.png" alt="Nuke Hunter"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/Nuclear_Missile.webp" alt="Nuclear Missiles"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/Patriotism.webp" alt="Patriotism"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            2.000$
          </td>
          <td style="text-align: left">
            + Horde Bonus<br>
            + Nuclear Missiles
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 10.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
  </tr>
</table>

#### Special Units

<table>
  <tr>
    <th>
      <img src="assets/hero.png" alt="Hero"/> Pilot
    </th>
    <th>
      <img src="assets/star.png" alt="Hero"/> Agent 47
    </th>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Pilot.webp" alt="Pilot"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            1.000$
          </td>
          <td style="text-align: left">
            + Hero Rank
          </td>
        </tr>
      </table>
    </td>
    <td>
      <table>
        <tr>
          <td style="text-align: center">
            <img width="122" height="98" src="assets/units/Jarmen_Kell.webp" alt="Agent 47"/>
          </td>
          <td style="text-align: left">
            <img width="60" height="48" src="assets/upgrades/Advanced_Training.webp" alt="Advanced Training"/>
            <img width="60" height="48" src="assets/upgrades/AP_Bullets.webp" alt="AP Bullets"/>
            <img width="60" height="48" src="assets/upgrades/Remote_Demo_Charge.webp" alt="Remote Charge"/>
            <img width="60" height="48" src="assets/upgrades/Pilot_Kill.webp" alt="Pilot Kill"/>
            <img width="60" height="48" src="assets/upgrades/Black_Lotus_Capture_Building.webp" alt="Capture Building"/>
            <img width="60" height="48" src="assets/upgrades/Disable_Building.webp" alt="Disable Building"/>
            <img width="60" height="48" src="assets/upgrades/Camouflage.webp" alt="Camouflage"/>
            <br>
            <img width="60" height="48" src="assets/upgrades/Chemical_Suits.webp" alt="Chemical Suits"/>
            <img width="60" height="48" src="assets/upgrades/Timed_Demo_Charge.webp" alt="Timed Charge"/>
            <img width="60" height="48" src="assets/upgrades/Detonate_Charges.webp" alt="Detonate Charges"/>
            <img width="60" height="48" src="assets/upgrades/Knife_Kill.webp" alt="Knife Kill"/>
            <img width="60" height="48" src="assets/upgrades/Cash_Hack.webp" alt="Cash Hack"/>
            <img width="60" height="48" src="assets/upgrades/Disable_Vehicle.webp" alt="Disable Vehicle"/>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            5.000$
          </td>
          <td style="text-align: left">
            + Climbing Abilities
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <ul>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Unique Unit</li>
        <li>Requires Research at the <i>Research Center</i></li>
        <li>Research Cost: 25.000$</li>
        <li>Requires <i>Strategy Center</i></li>
        <li>Produced at the <i>Barracks</i></li>
      </ul>
    </td>
  </tr>
</table>